
#	Makefile for robots

INCLUDES = -I.
SWITCHES = -DDEBUG
CPPFLAGS = $(INCLUDES) $(SWITCHES)
CFLAGS   = -Os -Wall -Wextra -Wpedantic
LDLIBS   = -lncurses

PROGRAMS = robots robotscore

all: $(PROGRAMS)

PREFIX = /usr/local
BINDIR = $(PREFIX)/bin

install: all
	install -d $(DESTDIR)$(BINDIR)/
	install $(PROGRAMS) $(DESTDIR)$(BINDIR)/

clean:
	$(RM) $(PROGRAMS)
