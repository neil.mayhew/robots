/*
 * Unix robots game
 *
 * N J H Mayhew - 06 Jan 86
 */

#include <curses.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <term.h>
#include <time.h>
#include <unistd.h>

typedef struct
{
	int x, y;
} Point;

typedef struct
{
	Point place;
	bool  alive;
} Robot;

#define MAXR 20000

Robot robot[MAXR];
Point me;

long seed;
bool debug;

Point max, min;

#define ME     'I'
#define ROBOT  '='
#define JUNK   '@'
#define EMPTY  ' '

int stake = 10;
long score = 0;
int startr = 0;
int numr = 0;

int alive;

#ifndef CTRL
#define CTRL(X) ((X) & 0x1F)
#endif

void cleanup(int);
void display();
void drawbox(int, int, int, int);
char getstatus(Point*);
void highscores();
void killed();
void newscreen();
void put(Point*, int);
void setstatus(Point*, int);

Point rpoint()
{
	Point p;

	p.x = min.x + lrand48() % (max.x - min.x);
	p.y = min.y + lrand48() % (max.y - min.y);

	return p;
}

void charge(Robot *rp)
{
	if (!rp->alive)
		return;

	if (getstatus(&rp->place) == EMPTY)
		put(&rp->place, EMPTY);

	if (rp->place.x > me.x)
		rp->place.x--;
	if (rp->place.x < me.x)
		rp->place.x++;
	if (rp->place.y > me.y)
		rp->place.y--;
	if (rp->place.y < me.y)
		rp->place.y++;

	switch (getstatus(&rp->place))
	{
	case JUNK:
		score += stake;
		break;
	case ROBOT:
		score += stake * 2;
		put(&rp->place, JUNK);
		alive--;
		break;
	case ME:
		killed();
		break;
	case EMPTY:
		put(&rp->place, ROBOT);
		alive++;
		break;
	}
}

void check(Robot *rp)
{
	if (getstatus(&rp->place) == ROBOT)
		setstatus(&rp->place, EMPTY);
	else
		rp->alive = false;
}

void update()
{
	int i;

	alive = 0;

	for (i = 0; i < numr; i++)
		check(&robot[i]);

	for (i = 0; i < numr; i++)
		charge(&robot[i]);
}

void redraw()
{
	int i;

	clearok(stdscr, TRUE);
	refresh();
	return;

	clear();

	for (i = 0; i < numr; i++)
		put(&robot[i].place, getstatus(&robot[i].place));

	put(&me, getstatus(&me));
}

void stand()
{
	struct timespec delay = {0, 0.1 * 1e9};

	while (alive > 0)
	{
		display();
		update();
		nanosleep(&delay, 0);
	}
}

void travel(int x, int y)
{
	put(&me, EMPTY);

	me.x += x;
	me.y += y;

	if (me.x < min.x)
		me.x = min.x;
	if (me.x >= max.x)
		me.x = max.x - 1;
	if (me.y < min.y)
		me.y = min.y;
	if (me.y >= max.y)
		me.y = max.y - 1;

	if (getstatus(&me) != EMPTY)
		killed();

	put(&me, ME);

	update();
}

void teleport()
{
	put(&me, EMPTY);

	do
	{
		me = rpoint();
	}
	while (getstatus(&me) != EMPTY);

	put(&me, ME);

	update();
}

void command(char c)
{
	switch (c)
	{
	case 'q':
		cleanup(0);
		break;
	case 's':
		stand();
		break;

	case 'y':
		travel(-1, -1);
		break;
	case 'u':
		travel(0, -1);
		break;
	case 'i':
		travel(1, -1);
		break;
	case 'h':
		travel(-1, 0);
		break;
	case 'j':
		travel(0, 0);
		break;
	case 'k':
		travel(1, 0);
		break;
	case 'n':
		travel(-1, 1);
		break;
	case 'm':
		travel(0, 1);
		break;
	case ',':
		travel(1, 1);
		break;

	case ' ':
		travel(0, 0);
		break;

	case 't':
		teleport();
		break;

	case CTRL('R'):
		redraw();
		break;

	default:
		flash();
		break;
	}
}

void cleanup(int n)
{
	char buf[100];

	clear();

	sprintf(buf, "SCORE: %ld", score);
	mvaddstr(1, 10, buf);

	highscores();

	mvaddstr(LINES - 2, 10, "Press any key to continue ...");
	getch();

	endwin();
	exit(n);
}

char** status;

void setstatus(Point *p, int c)
{
	status[p->y][p->x] = c;
}

char getstatus(Point *p)
{
	return status[p->y][p->x];
}

void setup()
{
	int i, j;

	srand48(seed);

	initscr();
	cbreak();
	noecho();

#ifdef NODEL
	delete_character = 0;
	insert_character = 0;
#endif

	signal(SIGINT, cleanup);

	min.x = 1;
	min.y = 1;
	max.x = COLS - 20;
	max.y = LINES - 1;

	status = (char**)calloc(LINES, sizeof(char*));

	for (j = 0; j < LINES; j++)
	{
		status[j] = (char*)calloc(COLS, sizeof(char));

		for (i = 0; i < COLS; i++)
		{
			status[j][i] = 0;
		}
	}

	if (!startr)
		startr = max.y * max.x / 138; /* 10 for a 24x80 screen */

	numr = startr;

	newscreen();
}

void newscreen()
{
	int i;
	Point p;

	if (numr > MAXR)
		numr = MAXR;

	alive = numr;

	clear();
	drawbox(min.y - 1, min.x - 1, max.y, max.x);

	mvaddstr(4 , max.x + 4, "SYMBOLS:");
	mvaddch( 6 , max.x + 4, 'I' | A_STANDOUT); addstr(" - you");
	mvaddstr(7 , max.x + 4, "= - robot");
	mvaddstr(8 , max.x + 4, "@ - junk heap");
	mvaddstr(10, max.x + 4, "MOVES:");
	mvaddstr(12, max.x + 4, "y u i");
	mvaddstr(13, max.x + 4, " \\|/");
	mvaddstr(14, max.x + 4, "h-j-k");
	mvaddstr(15, max.x + 4, " /|\\");
	mvaddstr(16, max.x + 4, "n m ,");
	mvaddstr(18, max.x + 4, "t - teleport");
	mvaddstr(19, max.x + 4, "s - last stand");
	mvaddstr(20, max.x + 4, "q - quit");

	if (debug)
	{
		char buf[80];

		sprintf(buf, "numr: %d stake: %d seed: %lu",
			numr, stake, seed);
		mvaddstr(max.y, 5, buf);
	}

	for (p.y = min.y; p.y < max.y; p.y++)
	for (p.x = min.x; p.x < max.x; p.x++)
		setstatus(&p, EMPTY);

	for (i = 0; i < numr; i++)
	{
		do
		{
			robot[i].place = rpoint();
		}
		while (getstatus(&robot[i].place) != EMPTY);

		robot[i].alive = true;

		put(&robot[i].place, ROBOT);
	}

	do
	{
		me = rpoint();
	}
	while (getstatus(&me) != EMPTY);

	put(&me, ME);
}

static char Usage[] =
	"Usage: %s [-s SEED] [-d] [NROBOTS]\n"
	"    -s SEED  Use SEED as the random seed\n"
	"    -d       Display debug information\n"
	"    -?       Show this help text\n"
	;

int main(int argc, char *argv[])
{
	int c;

	seed = time((long *)0);

	while ((c = getopt(argc, argv, "ds:?")) != EOF)
		switch (c)
		{
		case 's':
			seed = atol(optarg);
			break;
		case 'd':
			debug = true;
			break;
		default:
			fprintf(stderr, Usage, argv[0]);
			return 1;
		}

	if (optind < argc)
		startr = atoi(argv[optind++]);

	setvbuf(stdout, (char *)0, _IOFBF, BUFSIZ);

	setup();

	for (;;)
	{
		display();

		command(getch());
	}
}

void display()
{
	char buf[20];

	sprintf(buf, "Score: %ld", score);
	mvaddstr(2, max.x + 4, buf);

	move(me.y, me.x);
	refresh();

	if (alive == 0)
	{
		numr += startr;
		stake += 10;

		newscreen();

		display();
	}
}

void killed()
{
	mvaddstr(me.y, me.x,"*MUNCH*");
	refresh();

	sleep(2);

	cleanup(0);
}

void put(Point *p, int c)
{
	mvaddch(p->y, p->x, c);

	setstatus(p, c);

#ifdef CAREFUL
	refresh();
#endif
}

void drawbox(int fy, int fx, int ty, int tx)
{
	int y, x;

	for (y = fy + 1; y < ty; y++)
	{
		mvaddch(y, fx, '|');
		mvaddch(y, tx, '|');
	}
	for (x = fx + 1; x < tx; x++)
	{
		mvaddch(fy, x, '-');
		mvaddch(ty, x, '-');
	}

	mvaddch(fy, fx, '+');
	mvaddch(ty, fx, '+');
	mvaddch(fy, tx, '+');
	mvaddch(ty, tx, '+');
}

#define NSCORE 10
#define NSIZE  10

typedef struct
{
	char name[NSIZE+1];
	long score;
} Score;

Score score_table[NSCORE];

int score_rcmp(const void *p, const void *q)
{
	const Score *a = p, *b = q;
	long result = b->score - a->score;

	if (result < 0)
		return -1;
	if (result > 0)
		return 1;

	return 0;
}

char *name;
int place;

#ifdef DEBUG
char *fname = "SCORES";
#else
char *fname = "/usr/games/lib/robotscore";
#endif

#define BSIZ 40

void highscores()
{
	char buf[BSIZ];
	FILE *file;
	int i, n, y;

	/* Obtain name of player */
	if ((name = getlogin()) == NULL)
		name = "WHO??";

	/* Read in file */
	if ((file = fopen(fname, "r+")) == NULL)
		return;

	for (i = 0; i < NSCORE && fgets(buf, BSIZ, file); i++)
		if (sscanf(buf, "%10s%ld",
			score_table[i].name, &score_table[i].score) != 2)
			return;

	fclose(file);

	n = i;

	/* Add to list */
	qsort(score_table, n, sizeof(score_table[0]), score_rcmp);

	for (i = 0; i < n; i++)
		if (score > score_table[i].score)
			break;

	place = i;

	if (n < NSCORE)
		n++;

	for (i = n - 1; i > place; i--)
		score_table[i] = score_table[i-1];

	if (place < n)
	{
		strncpy(score_table[place].name, name, NSIZE);
		score_table[place].score = score;
	}

	/* Print list */
	y = 3;
	mvaddstr(y++, 10, "HIGH SCORES TO DATE:");
	y++;

	for (i = 0; i < n; i++)
	{
		char line[100];
		sprintf(line, "   %2d    %-*.*s %6ld\n", i, NSIZE, NSIZE,
			score_table[i].name, score_table[i].score);
		mvaddstr(y++, 10, line);
	}
	y++;

	if (place == 0)
		mvaddstr(y, 10, "Congratulations, Maestro!");

	/* Write out file */
	if ((file = fopen(fname, "w")) == NULL)
		return;

	for (i = 0; i < n; i++)
		fprintf(file, "%.*s %ld\n", NSIZE,
			score_table[i].name, score_table[i].score);

	fclose(file);
}
