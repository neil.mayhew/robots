{ stdenv, lib, ncurses }:

stdenv.mkDerivation rec {

  pname = "robots";
  version = "0.9.0";

  src = lib.cleanSource ./.;

  buildInputs = [ ncurses ];

  makeFlags = "PREFIX=$(out)";

  meta = {
    description = "A terminal-based robots game";
    license = lib.licenses.gpl3;
    maintainers = [ "Neil Mayhew <neil@mayhew.name>" ];
  };
}
