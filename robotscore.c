/*
 * Score printer for Unix robots game
 *
 * N J H Mayhew - 22 Jan 86
 */

#include <stdlib.h>
#include <stdio.h>

#define NSCORE 10
#define NSIZE  10

typedef struct
{
	char name[NSIZE+1];
	long score;
} Score;

Score score_table[NSCORE];

int score_rcmp(const void *p, const void *q)
{
	const Score *a = p, *b = q;
	long result = b->score - a->score;

	if (result < 0)
		return -1;
	if (result > 0)
		return 1;

	return 0;
}

#ifdef DEBUG
char *fname = "SCORES";
#else
char *fname = "/usr/games/lib/robotscore";
#endif

#define BSIZ 40

int main()
{
	char buf[BSIZ];
	FILE *file;
	int i, n;

	/* Read in file */
	if ((file = fopen(fname, "r")) == NULL)
		return 0;

	for (i = 0; i < NSCORE && fgets(buf, BSIZ, file); i++)
		if (sscanf(buf, "%10s%ld",
			score_table[i].name, &score_table[i].score) != 2)
			return 0;

	fclose(file);

	n = i;

	/* Sort list */
	qsort(score_table, n, sizeof(score_table[0]), score_rcmp);

	/* Print list */
	printf("\n");

	for (i = 0; i < n; i++)
		printf("   %2d    %-*.*s %5ld\n", i, NSIZE, NSIZE,
			score_table[i].name, score_table[i].score);
}
